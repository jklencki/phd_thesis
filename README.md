Table of contents:
*******************

I. Introduction

II. Dynamical formation of black hole low-mass X-ray binaries in the field: an alternative to the common envelope, J. Klencki et al., MNRAS, 469, 3 (2017) **

III. Impact of inter-correlated initial binary parameters on double black hole and neutron star mergers, J. Klencki et al., A&A, 619, A77 (2018)

IV. Evolutionary roads leading to low effective spins, high black hole masses, and O1/O2 rates for LIGO/Virgo binary black holes, K. Belczynski, J. Klencki, et al., A&A, 636, A104 (2020)

V. Massive donors in interacting binaries: effect of metallicity, J. Klencki et al., A&A, 638, A55 (2020)

VI. It has to be cool: Supergiant progenitors of binary black hole mergers from common-envelope evolution, J. Klencki et al., A&A, 645, A54 (2021)

VII. Partial envelope stripping and nuclear-timescale mass transfer from evolved supergiants at low metallicity, J. Klencki et al., to be submitted to A&A

VIII. Summary

plus Research Data Management Statement

*******************
**This chapter was published before the start of the actual PhD research, but has been included in this thesis because it thematically complements the rest of the contents.
